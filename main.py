import json

import requests

with open("settings.json", "r") as settings_file:
    settings = json.load(settings_file)
with open("settings_environment.json") as settings_environment_file:
    env_settings = json.load(settings_environment_file)


class OpenWeatherAPI:
    def __init__(self):
        self.url = "%(url_base)sdata/2.5/weather?id=593116&appid=%(api_key)s" % {
            "url_base": settings["API_ROOT"],
            "api_key": env_settings["API_KEY"]
        }

    def get_current_temperature(self):
        response = requests.get(self.url)
        return json.loads(response.text)["main"]["temp"]


def kelvin_to_celsius(kelvins: float) -> float:
    """
    Converts kelvins to degrees Celsius
    :param kelvins: temperature Kelvins
    :return: temperature in Celsius
    """
    return kelvins - 273


def main():
    api = OpenWeatherAPI()
    current_temperature = api.get_current_temperature()
    celsius_temprature = kelvin_to_celsius(current_temperature)
    print(celsius_temprature)


if __name__ == "__main__":
    main()
